 <h1 class="curproject-name"> java-book </h1>

> [项目仓库]( https://gitlab.com/xuyq123/java-book )

此仓库保存java相关书籍，涉及java基础、虚拟机、算法、数据库、linux、代码重构等等。


> 备份网址

- [阿里云盘]( https://www.aliyundrive.com/drive/ ) &ensp; [坚果云]( https://www.jianguoyun.com )  &ensp; [百度网盘]( https://pan.baidu.com/ )
- [钉钉云盘]( https://im.dingtalk.com/ )  &ensp; [阿里云个人邮箱]( https://mail.aliyun.com/alimail/auth/login )
- [gitlab]( https://gitlab.com/xuyq123/java-book ) &ensp; [gitee]( https://gitee.com/xy180/java-book ) &ensp; [github]( https://github.com/scott180/java-book ) &ensp; [csdn_code]( https://codechina.csdn.net/xu180/java-book )  &ensp; [coding]( https://xyqin.coding.net/public/my/java-book/git/files )   	

